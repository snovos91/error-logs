define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'mainGrid',
  'views/headerView',
  'views/contentView',
  'collections/errorsCollection',
  'collections/serversCollection',
  'models/filtersModel',
  'backbone.paginator'
],

function ($, _, Backbone, Marionette, Layout, HeaderView, ContentView, ErrorsCollection, ServersCollection, FiltersModel) {

  'use strict';

  return Backbone.Router.extend({

    initialize : function () {
      this.layout = new Layout();
      this.layout.render();
    },

    routes : {
      '' : 'showMain',
      'main' : 'showMain'
    },

    showMain : function () {
      var serversCollection = new ServersCollection();
      var filtersModel = new FiltersModel();
      var errorsCollection = new ErrorsCollection(null, {filtersModel:filtersModel,serverList:serversCollection});
      var headerView = new HeaderView({model:filtersModel,serverList:serversCollection});
      var contentView = new ContentView( {collection:errorsCollection, filtersModel:filtersModel});
      this.layout.gridHead.show(headerView);
      this.layout.gridContent.show(contentView);
      errorsCollection.pager();
      serversCollection.fetch({
        error : function () {
        window.alert('Data load failed');
        },
        success : function () {
          filtersModel.set('servers', serversCollection.toJSON());
        }
      });
    }
  });

});

