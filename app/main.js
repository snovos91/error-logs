require([
  'jquery',
  'underscore',
  'backbone',
  'app',
  'router',
  'override',
  'bootstrap'
],

function ($,_, Backbone, app, Router) {

  'use strict';

  $(function () {
    app.router = new Router();
    app.vent = _.extend({}, Backbone.Events);
    app.start();
    Backbone.history.start();
  });

});
