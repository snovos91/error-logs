define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app',
  'moment',
  'backbone.paginator'
],

function ($, _, Backbone, Marionette, app, moment) {

  'use strict';

  return Backbone.Paginator.requestPager.extend({

    lastPage : false,

    initialize : function (models, options) {
      this.filtersModel = options.filtersModel;
      this.serverList = options.serverList;
      this.listenTo(app.vent, 'pager', this.fetchData);
    },

    paginator_core : {
      // the type of the request (GET by default)
      type : 'GET',

      // the type of reply (jsonp by default)
      dataType : 'json',

      // the URL (or base URL) for the service
      // if you want to have a more dynamic URL, you can make this a function
      // that returns a string
      url : 'data.json'
    },

    paginator_ui : {
      // the lowest page index your API allows to be accessed
      firstPage : 1,

      // which page should the paginator start from
      // (also, the actual page the paginator is on)
      currentPage : 1,

      // how many items per page should be shown
      perPage : 20,

      // a default number of total pages to query in case the API or
      // service you are using does not support providing the total
      // number of pages for us.
      // 10 as a default in case your service doesn't return the total
      totalPages : 10
    },

    formatDate : function (modelDate) {
      var date = new Date(modelDate);
      if (moment(date).isValid()) {
        return date.toISOString();
      }
      return '';
    },

    server_api : {
      severity : function () {
        return this.filtersModel.get('severity').join(',');
      },
      endDate : function () {
        return this.formatDate(this.filtersModel.get('endDate'));
      },
      startDate : function () {
        return this.formatDate(this.filtersModel.get('startDate'));
      },
      servers : function () {
        return this.filtersModel.get('servers').join(',');
      },
      type : function () {
        return this.filtersModel.get('type');
      },
      sort : function () {
        return this.filtersModel.get('sort');
      },
      direction : function () {
        return this.filtersModel.get('direction');
      }
    },

    parse : function (response) {
      var from = (this.currentPage - 1) * this.perPage;
      var data = response.slice(from, from + this.perPage);
      if (data.length < this.perPage) {
        this.lastPage = true;
      }
      return data;
    },

    isLastPage : function () {
      return this.lastPage;
    },

    fetchData : function () {
      this.currentPage = 1;
      this.pager();
    }

  });

});
