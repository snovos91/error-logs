define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'collections/serversCollection',
  'moment'
],

function ($, _, Backbone, Marionette, ServersCollection, moment) {

  'use strict';

  return Backbone.Model.extend({

    defaults : {
      startDate : function () {
        var date = new Date();
        date.setDate(date.getDate() - 30);
        return moment(date).format('MMM-DD-YYYY');
      },
      endDate : moment(new Date()).format('MMM-DD-YYYY'),
      severity : ['error','warning'],
      type : 'indexation',
      servers : [],
      sort : 'date',
      direction : 1
    }
  });

});
