define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'views/rowView',
  'app'
],

function ($, _, Backbone, Marionette, RowView, app) {

  'use strict';

  return Marionette.CompositeView.extend({

    template : 'content',
    itemView : RowView,
    itemViewContainer : 'tbody',

    events : {
      'click [data-load-more]' : 'loadMore',
      'click [data-sort]' : 'sortData'
    },

    ui : {
      sortField : '[data-sort]'
    },
    bindings : {
      '[data-sort-field]' : {
        'observe' : 'sort',
        'updateView' : false
      },
      '[data-load-more]' : {
        observe : 'isLastPage',
        updateView : false,
        visible : function (val) {
          return !val;
        }
      }
    },

    initialize : function (options) {
      this.stickitModel = new Backbone.Model();
      this.listenTo(this.collection, 'sync', this.updateStickitModel);
      this.listenTo(options.filtersModel, 'change', this.showSortedField);
    },

    sortData : function (e) {
      var current = $(e.currentTarget).attr('data-sort');
      var sortBy = this.options.filtersModel.get('sort');
      var direction = this.options.filtersModel.get('direction');
      this.options.filtersModel.set({
        sort : current,
        direction : current === sortBy ? direction * -1 : 1
      });
      app.vent.trigger('pager');
    },

    showSortedField : function () {
      var sortBy = this.options.filtersModel.get('sort');
      var direction = this.options.filtersModel.get('direction');
      this.ui.sortField.each(function (index, el) {
        var $el = $(el);
        $el.find('i').removeClass();
        if ($el.attr('data-sort') === sortBy) {
          $el.find('i').addClass('icon-chevron-' + (direction === 1 ? 'up' : 'down'));
        }
      });
    },

    updateStickitModel : function () {
      this.stickitModel.set('isLastPage', this.collection.isLastPage());
      this.stickitModel.set('sort', this.options.filtersModel.get('sort'));
    },

    onRender : function () {
      this.stickit(this.stickitModel);
    },

    loadMore : function (e) {
      e.preventDefault();
      this.collection.requestNextPage({update:true, remove:false});
    }
  });

});
