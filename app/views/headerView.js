define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app',
  'views/filterView',
  'datepicker',
  'backbone.stickit'
],

function ($, _, Backbone, Marionette, app, FilterView) {

  'use strict';

  return Marionette.ItemView.extend({

    template : 'header',

    ui : {
      filter : '[data-filter]'
    },
    initialize : function (options) {
      var serverList = options.serverList;
      this.filterView = new FilterView({model:this.model, serverList:serverList});
    },

    onRender : function () {
      this.ui.filter.prepend(this.filterView.render().el);
    }
  });

});
