define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'moment'
],

function ($, _, Backbone, Marionette, moment) {

  'use strict';

  return Marionette.ItemView.extend({

    tagName : 'tr',

    template : 'row',

    serializeData : function () {
      var model = this.model.toJSON();
      return _.extend([], model, {
        formattedDate : moment(model.date).format('MMM-DD-YYYY HH:mm')
      });
    }
  });

});
