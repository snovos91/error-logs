define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'app',
  'collections/errorsCollection',
  'collections/serversCollection'
],

function ($, _, Backbone, Marionette, app, ErrorsCollection, ServersCollection) {

  'use strict';

  return Marionette.CompositeView.extend({

    template : 'filter',

    events : {
      'submit' : 'submitForm',
      'click .dropdown-menu' : 'stopBubbling',
      'click .reset' : 'resetForm'
    },

    ui : {
      dateFrom : '[data-date-start]',
      dateTo : '[data-date-end]',
      dataDropdown : '[data-toggle="dropdown"]',
      dataDatepicker : '[data-datepicker]',
      dataOptionServer : '[data-option-server]'
    },

    modelState : {},

    bindings: {
      '[data-date-start]' : 'startDate',
      '[data-date-end]' : 'endDate',
      '[data-option-severity]' : 'severity',
      '[data-option-type]' : 'type',
      '[data-option-server]' : {
          observe : 'servers',
          afterUpdate : function () {
            this.ui.dataOptionServer.children('option').prop('selected', false);
          },
          selectOptions : {
            collection : function () {
              return this.model.get('servers');
            },
            labelPath : 'server',
            valuePath : 'server'
          }
      }
    },

    initialize : function () {
      this.listenTo(app.vent, 'submitForm', this.submitForm);
    },

    preventDatepickerClick : function (dataDatePicker) {
      var stopEvent = function (e) {
        e.preventDefault();
        e.stopPropagation();
      };
      dataDatePicker.on('show', function () {
        $('.datepicker')
          .off('click.filterView')
          .on('click.filterView', stopEvent);
      });
    },

    initDatePicker : function (datePickerObj) {
      datePickerObj.datepicker();
    },

    onRender : function () {
      this.stickit();
      this.initDatePicker(this.ui.dateFrom);
      this.initDatePicker(this.ui.dateTo);
      this.preventDatepickerClick(this.ui.dataDatepicker);
    },

    stopBubbling : function (e) {
      e.stopPropagation();
    },

    resetForm : function () {
      this.model.clear().set(this.modelState);
      this.closeDropDown();
    },

    closeDropDown : function () {
      this.ui.dataDropdown.dropdown('toggle');
    },

    submitForm : function (e) {
      e.preventDefault();
      this.closeDropDown();
      this.modelState = this.model.toJSON();
      app.vent.trigger('pager');
    }
  });

});
