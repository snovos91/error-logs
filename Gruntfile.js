module.exports = function(grunt) {

  var connect = require('connect');

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress : true,
          yuicompress : true,
          optimization : 2
        },
        files : {
          'assets/stylesheets/style.css' : 'assets/less/style.less',
          'assets/stylesheets/bootstrap.css' : 'assets/less/bootstrap.less',
          'assets/stylesheets/datepicker.css' : 'assets/less/datepicker.less'
        }
      }
    },
    watch: {
      styles : {
        files : ['assets/less/*.less'],
        tasks : ['less'],
        options: {
          nospawn : true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('connect', function () {
    grunt.log.writeln('Starting static web server on port 8001');
    connect(connect.static('../error_logs')).listen(8001);
  });

  grunt.registerTask('default', ['less', 'connect', 'watch']);


};
